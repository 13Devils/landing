$(document).ready(function () {
  //Initial bootstrap tooltip
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  //Initial custom select - https://developer.snapappointments.com/bootstrap-select/
  $(function () {
    $('.selectpicker').selectpicker('selectAll');

    $(".selectpicker").on("change", function () {
      var dataLanguage = $("option[value=" + $(this).val() + "]", this).attr('data-language');
      $('body').attr('class', '').toggleClass(dataLanguage)
    });
  });

  //Bootstrap validation form
  bootstrapValidate('#emailEn', 'email:Enter a valid E-Mail!', function (isValid) {
    if (isValid) {
      $('.launch-form .btn-submit').removeAttr("disabled");
      $('.launch-form .form-group-launch').addClass('form-group-success')
    } else {
      $('.launch-form .btn-submit').attr("disabled", "disabled");
      $('.launch-form .form-group-launch').removeClass('form-group-success')
    }
  });
  bootstrapValidate('#emailRu', 'email:Введите корректный E-Mail!', function (isValid) {
    if (isValid) {
      $('.launch-form .btn-submit').removeAttr("disabled");
      $('.launch-form .form-group-launch').addClass('form-group-success')
    } else {
      $('.launch-form .btn-submit').attr("disabled", "disabled");
      $('.launch-form .form-group-launch').removeClass('form-group-success')
    }
  });
  bootstrapValidate('#emailEs', 'email:Enter a valid E-Mail!', function (isValid) {
    if (isValid) {
      $('.launch-form .btn-submit').removeAttr("disabled");
      $('.launch-form .form-group-launch').addClass('form-group-success')
    } else {
      $('.launch-form .btn-submit').attr("disabled", "disabled");
      $('.launch-form .form-group-launch').removeClass('form-group-success')
    }
  });


  //Submit form
  $('#launch-form').submit(function (e) {
    e.preventDefault();
    $.ajax({
      url: 'mailer.php',
      type: 'post',
      data: $('#launch-form').serialize(),
      success: function () {
        $('#launch-modal').modal('hide')
        $('#welcome-modal').modal('show')
      }
    });
  });




  var videoSrc = $("#video-modal iframe").attr("src");

  $('#video-modal').on('show.bs.modal', function () { // on opening the modal
    // set the video to autostart
    $("#video-modal iframe").attr("src", videoSrc + "&amp;autoplay=1");
  });

  $("#video-modal").on('hidden.bs.modal', function (e) { // on closing the modal
    // stop the video
    $("#video-modal iframe").attr("src", null);
  });

});